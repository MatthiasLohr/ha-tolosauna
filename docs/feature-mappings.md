# Feature Mappings

This table lists the mapping of supported TOLO Sauna settings/status information to Home Assistant components.

| TOLO Realm   | TOLO Feature         | HA Platform  | HA Feature          | Implemented |
| ------------ | -------------------- | ------------ | ------------------- | ----------- |
| Main Control | Power on/off         | Climate      | On/Off              | ✓           |
| Main Control | Fan on/off           | Climate      | Fan Mode            | ✓           |
| Main Control | Aroma Therapy on/off |              |                     | ✗           |
| Main Control | Lamp on/off          | Light        | On/Off              | ✓           |
| Main Control | Sweep on/off         |              |                     | ✗           |
| Main Control | Salt Bath on/off     |              |                     | ✗           |
| Main Control | Lamp - Next Color    | Button       | Button Event Action | ✓           |
| Status       | Current Temperature  | Climate      | Current Humidity    | ✓           |
| Status       | Current Humidity     | Climate      | Current Temperature | ✓           |
| Status       | Calefaction          |              |                     | ✗           |
| Status       | Water Level          | Sensor       | Native Measurement  | ✓           |
| Status       | Flow in/out          | BinarySensor | On/Off              | ✓           |
| Status       | Device model         | *Generic*    | Device Info         | ✓           |
| Status       | Power Timer          |              |                     | ✗           |
| Status       | Salt Spray Timer     |              |                     | ✗           |
| Status       | Fan Timer            |              |                     | ✗           |
| Status       | Tank Temperature     | Sensor       | Native Measurement  | ✓           |
| Setting      | Target Temperature   | Climate      | Target Temperature  | ✓           |
| Setting      | Target Humidity      | Climate      | Target Humidity     | ✓           |
| Setting      | Power Timer          |              |                     | ✗           |
| Setting      | Salt Bath Timer      |              |                     | ✗           |
| Setting      | Aroma Therapy Slot   | Select       | Option              | ✗           |
| Setting      | Sweep Timer          |              |                     | ✗           |
| Setting      | Lamp Mode            | Select       | Option              | ✓           |
